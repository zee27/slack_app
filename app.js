const express = require('express');
const path = require('path');
const app = express();
const cors = require('cors');
const morgan = require('morgan');
const fileUpload = require('express-fileupload');
var bodyParser = require('body-parser')

require("./nodemon_config");
// mongoose db configuration
require("./config/db_config");
const Apis = require("./Routes/Apis");
const user_apis = require("./Routes/user");

// core middleware for origin and headers
app.use(cors());

// Morgan middleware to log requestso on console
app.use(morgan('dev'));
// parse application/json
app.use(bodyParser.json());

// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({
    extended: false
}))

//// file upload middle ware
app.use(fileUpload());
app.use('/uploads', express.static('./uploads'));

//build static folder path
app.use(express.static(path.join(__dirname, 'public/dist')))


// port
const PORT = process.env.PORT || '3000';


app.get("/", function (req, res, next) {
    res.sendFile(path.join(__dirname, 'public/dist/index.html'));
})

app.use("/apis", Apis);
app.use("/user", user_apis);

// Error handling middleware
app.use((error, req, res, next) => {

    res.status(error.status || 500);
    res.json({
        error: {
            message: error.message
        }
    });
});


app.listen(PORT, () => {
    console.log('=================================');
    console.log("App is running on port " + PORT);
    console.log('=================================');
});


module.exports = app;