const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const User = require('./user');
const Comment = require('./comment');


const PostSchema = new Schema({

    content: {
        type: String,
        default: null
    },
    files: [{
        type: String,
        default: null
    }],
    _posted_by: {
        type: Schema.Types.ObjectId,
        ref: 'User'
    },
    likes_count: {
        type: Number,
        default: 0,
    },
    liked_by: [{
        type: Schema.Types.ObjectId,
        ref: 'User',
    }],
    comments_count: {
        type: Number,
        default: 0
    },
    comments: [{
        type: Schema.Types.ObjectId,
        ref: 'Comment'
    }],
    created_date: {
        type: Date,
        default: Date.now
    }
});


const Post = mongoose.model('Post', PostSchema);
module.exports = Post;