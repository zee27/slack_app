const User = require('../models/user');
const Auth = require('../models/auth');
const helper_password = require('../helpers/password');
const jwt = require('jsonwebtoken');


module.exports = {
    signin(req, res, next) {

        const email = req.body.email;
        const password = req.body.password;

        User.findOne({
                'email': email,
            })
            .then((user) => {
                // if user dose not exists
                if (user == null) {
                    res.status(404).json({
                        message: "User not found,Invalid Email or password!"
                    });
                }
                // dcrypting plain password
                helper_password.dcryptPassword(user.password, password)
                    .then((response) => {

                        user.password = null;

                        // if password maches
                        if (response.response) {

                            const payload = {
                                id: user._id
                            };

                            //gernating new token to assign new signin user
                            const token = jwt.sign(payload, process.env.JWT_TOKEN);

                            let auth = {
                                token: token,
                                is_signin: true,
                                _user: user
                            };

                            user.password = null;

                            Auth.create(auth)
                                .then((auth) => {
                                    res.status(200).json({
                                        message: "Singin successfull!",
                                        user: user,
                                        token: token
                                    });
                                })
                                // auth error handling
                                .catch((err) => {
                                    res.status(500).json({
                                        message: "something went wrong in auth | internal server error!",
                                        err: err
                                    });
                                })

                        } else {
                            res.status(404).json({

                                message: "User not found, Invalid Email or password",
                            });
                        }
                    })
                    .catch((err) => {
                        res.status(500).json({
                            message: err.message,
                            err: err
                        });
                    })

            }).catch((err) => {
                res.status(500).json({
                    message: "something went wrong | internal server error!",
                    err: err
                });
            })
    },
    signup(req, res, next) {
        console.log("=============User signup=================");

        const data = req.body;

        // encrypting plain password
        helper_password.ecnryptPassword(data.password)
            .then((password) => {
                data.password = password.encryptedPassword;

                // storing new user in database
                User.create(data)
                    .then((user) => {
                        res.status(200).json({
                            user: user,
                            message: "user has created successfully!"
                        });
                    })
                    // error handling for user data storing
                    .catch((err) => {
                        res.status(501).json({
                            err: err,
                            message: "Something went wrong with database | internal server error"
                        });
                    })
            })
            // error handling for user password encryption
            .catch((err) => {
                res.status(500).json({
                    err: err,
                    message: "Something went wrong with database | internal server error"
                });
            })
    },
    // Route to signout user
    signout(req, res, next) {

        const user_id = req.body.user_id;

        Auth.findOneAndUpdate({
                _user: user_id,
                token: req.headers.authorization.split(' ')[1]
            }, {
                $set: {
                    token: null,
                    is_signin: false
                },
            }, {
                new: true
            })
            .then((auth) => {
                res.status(200).json({
                    message: "User signout successfully!",
                    auth: auth
                });
            })
            .catch((err) => {
                res.status(500).json({
                    message: "Something went wrong with database | internal server error",
                    err: err
                });

            })

    },
    // Route to get all users data
    getUsers(req, res, next) {

        const page = req.params.page;
        User.find({})
            .limit(5)
            .skip(page * 5)
            .sort('-_id')
            .then((users) => {
                res.status(200).json({
                    message: "Users found successfully!",
                    users: users
                });
            })
            .catch((err) => {
                res.status(500).json({
                    message: "Something went wrong with database | internal server error",
                    err: err
                });
            })
    },
    checkUserName(req, res, next) {

        const user_name = req.body.userName;

        User.findOne({
                userName: user_name
            })
            .then((user) => {

                if (!user) {
                    res.status(200).json({
                        message: "User name is available",
                        status: '200'
                    });
                } else {
                    res.status(200).json({
                        message: "user name has already taken,please try another one.",
                        status: '404'
                    })
                }
            })
            .catch((err) => {

                res.status(500).json({
                    message: "somethig is went wrong| internal server error!",
                    err: err
                })
            })
    }
}