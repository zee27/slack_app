import { AbstractControl } from "@angular/forms/src/model";
import { ValidationErrors } from "@angular/forms/src/directives/validators";


export class PasswordValidators {
  static PasswordMatch(control: AbstractControl) : ValidationErrors | null {
    if((control.value as string).indexOf(' ') >= 0 )
    return {cannotContainSpace: true};
    
    return null;
  }
}
