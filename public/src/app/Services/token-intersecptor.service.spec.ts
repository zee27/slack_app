import { TestBed, inject } from '@angular/core/testing';

import { TokenIntersecptorService } from './token-intersecptor.service';

describe('TokenIntersecptorService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [TokenIntersecptorService]
    });
  });

  it('should be created', inject([TokenIntersecptorService], (service: TokenIntersecptorService) => {
    expect(service).toBeTruthy();
  }));
});
