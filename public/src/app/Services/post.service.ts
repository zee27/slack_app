import { Injectable } from '@angular/core';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';
@Injectable({
  providedIn: 'root'
})
export class PostService {

  constructor(private _http: HttpClient) { }
  BASE_URL = environment.BASE_URL;

  getPosts() {
    return this._http.get<any>(this.BASE_URL + '/user/post/0');
  }
}
