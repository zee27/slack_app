import { Injectable } from '@angular/core';

import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';



@Injectable({
  providedIn: 'root'
})
export class AuthService {

  BASE_URL = environment.BASE_URL;

  constructor(private _http: HttpClient) { }

  signin(user) {
    return this._http.post<any>(this.BASE_URL + '/user/signin', user);

  }

  signup(user) {
    console.log('user in service: ', user);

    // return this._http.post<any>(this.BASE_URL + '/user/signup', user);
    return this._http.post<any>(this.BASE_URL + '/user/signup', user);
  }
  checkUserName(user_name) {
    // return this._http.post<any>(this.BASE_URL + '/user/signup', user);
    return this._http.post<any>(this.BASE_URL + '/user/checkUserName', user_name);
  }

  getToken() {
    return localStorage.getItem('token');
  }
}
