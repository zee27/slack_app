const jwt = require('jsonwebtoken');
const User = require('../models/user');
const Auth = require('../models/auth');


module.exports = {

    veryfiyJwtToken(req, res, next) {

        if (!req.headers.authorization) {
            res.status(401).json({
                message: "Unauthorizated Request!"
            });
        }
        const token = req.headers.authorization.split(' ')[1];
        jwt.verify(token, process.env.JWT_TOKEN, (err, decoded) => {

            if (err) {
                res.status(401).json({
                    message: "Unauthorizated Request | Invalid token"
                });
            }
            if (!decoded) {
                res.status(401).json({
                    message: "Unauthorizated Request | Invalid token"
                });
            }

            req.id = decoded.id;
            next();
        });
    },

    isLogin(req, res, next) {

        Auth.find({
                _user: req.id,
                is_signin: true,
                token: req.headers.authorization.split(' ')[1]
            })
            .then((auth) => {
                if (auth == null || auth.length < 1) {
                    res.status(401).json({
                        message: "Unauthorizated Request | Invalid token"
                    });
                }
                // go to UserController
                next();
            })
            .catch((err) => {
                res.status(500).json({
                    message: "something wenet wrong | Internal server error"
                });
            })
    }
}