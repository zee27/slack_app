const uploadFile = require('express-fileupload');

module.exports = {

    fileHander(req, res, next) {

        console.log("=================FILE HANDLER==================");

        if (!req.files) {
            next();
        }

        let folder = './uploads/';

        req.body.files = new Array();

        // multile files handling
        if (req.files.attachments.length) {
            req.files.attachments.forEach(currentFile => {

                const fileName = Date.now().toString() + currentFile.name;
                req.body.files.push(fileName);

                currentFile.mv(folder + fileName, (err) => {
                    if (err) {
                        res.status(500).json({
                            message: "file upload error ocured!"
                        });
                    }
                });
            });
            next();
        }
        // single file handling
        else {

            const currentFile = req.files.attachments;
            const fileName = Date.now().toString() + currentFile.name;
            req.body.files.push(fileName);

            currentFile.mv(folder + fileName, (err) => {
                if (err) {
                    res.status(500).json({
                        message: "file upload error ocured!"
                    });
                }
            });
            next();
        }


    }
}